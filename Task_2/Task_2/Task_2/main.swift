//
//  main.swift
//  Task_2
//
//  Created by кмв on 21.09.17.
//  Copyright © 2017 кмв. All rights reserved.
//

import Foundation
//print(" ---------------------------------task_2_1-------------------------------")
//1
var z: Int8 = 12
print(z)
//2
var z1: Int8 = -100
print(z1)
//3
var hex: Int = 0x80
print(hex)
//4
var min16 : Int16 = Int16.min
print(min16)
//5
var max64: Int64 = Int64.max
print(max64)
//6
var p1:Float = 10235.34
print(p1)
var p2:Float = 10.23534
print(p2)
//7
var a: Character = "a"
print(a)
//8
print("Hello World")
//9
var t: Bool = true
print(t)
//10
let tw = "12"
let Twelve = Int(tw)
print(Twelve!)
let tw1 = (12,"twelve")
print (tw1)

//---------------------------------------------------------

print(" ---------------------------------task_2_2-------------------------------")
//1
var d: String = "Hello"
print(d.characters.count)
//2
var k: String = "Milion"
print(k.replacingOccurrences(of: "i", with: "u"))
//3
var str: String = "Hello my World"
str.remove(at: str.index (str.startIndex, offsetBy: 3))//3
str.remove(at: str.index (str.startIndex, offsetBy: 5))//7
str.remove(at: str.index (str.startIndex, offsetBy: 7))//10
print (str)
//4
var m1 = "hello"
var m2 = " world"
print(m1 + m2)
//5
let str1: String = ""
print(str1.isEmpty)
//6
let zm: Character = "."
m1.append(zm)
print(m1)
//7
var s = m1.hasPrefix("hello")
print(s)
//8
var str5 = ("My World")
var s1 = str5.hasSuffix("World")
print(s1)
//9
var str3 = "hello my world"
str3.insert("-", at: str3.index(str.startIndex, offsetBy: 9))
print(str3)
//10
var str2: String = "Thus us"
var str4 = "It is"
var  str6 = str2.replacingOccurrences(of: str2, with: str4)
print(str6)
//11
var st = "hello my virtual world!"
let characters = Array(st.characters)
print("10 = \(characters[10]) , 15 = \(characters[15]) ")
//12
var ran = characters[10..<15]
print(ran)
print(" ---------------------------------task_2_3-------------------------------")
//1
var integerNumber : Int?
//2
var decimalNumber : Double?
//3
integerNumber = 10
//4
var i = 0
while (i<=9) {
integerNumber! += 1
    i += 1
}
print(integerNumber!)
//5
print(-(integerNumber!))
//6
decimalNumber = Double(integerNumber!)
print(decimalNumber!)
//7
var pairOrValues = (integerNumber, decimalNumber)
//8
pairOrValues = (nil, 123.0)
let (intN, decN) = pairOrValues
if intN == nil{
    print("nil")
}
else{
    print(intN!)
}
//9
if decN == nil{
    print("nil")
}
else{
    print(decN!)
}
//10
if var number = decimalNumber{
    print("number = \(number)")
}
else
{
    print("nil")
}
